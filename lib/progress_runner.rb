require 'open3'
require 'tty-spinner'
require './lib/ansi.rb'

class ProgressRunner
  attr_reader :title, :start_time

  def initialize(command, title: nil, silence_stdout: true)
    @command = command
    @title = title || command
    @silence_stdout = silence_stdout
  end

  def execute(dry_run: false, verbose: false)
    @dry_run = dry_run
    @verbose = verbose
    @start_time = Time.now

    puts "> #{command}" if verbose?

    run_command_with_spinner
  end

  def command
    if dry_run?
      "echo #{@command}"
    else
      @command
    end
  end

  def silence_stdout
    if dry_run?
      false
    else
      @silence_stdout
    end
  end

  private

  def dry_run?
    @dry_run
  end

  def verbose?
    @verbose
  end

  def spinner
    @spinner ||= TTY::Spinner.new(":spinner \e[1m#{title}\e[0m:time_elapsed",
                                  format: :dots,
                                  error_mark: ANSI::RED % '✖',
                                  success_mark: "\e[1m\e[32m✓\e[0m\e[0m")
  end

  def duration
    Time.now - start_time
  end

  def duration_header
    "(#{duration.round(2)} sec)"
  end

  def run_command_with_spinner
    Open3.popen3(command) do |_, stdout, stderr, waiter|
      stdout_output = ''
      stderr_output = ''

      loop do
        sleep(0.1) unless dry_run?

        update_spinner

        # Not reading output combined with large enough output can lead to the
        # thread being blocked, thus we read the output in chunks whenever
        # possible.
        write_if_ready(stdout, stdout_output)
        write_if_ready(stderr, stderr_output)

        break unless waiter.alive?
      end

      status = waiter.value
      handle_output(status, stdout_output, stderr_output)
    end
  end

  def handle_output(status, stdout_output, stderr_output)
    if status.success?
      stop_spinner(:success) unless dry_run?

      puts_if_not_empty($stdout, stdout_output) unless silence_stdout

      [stdout_output, stderr_output]
    else
      stop_spinner(:error)

      puts_if_not_empty($stdout, stdout_output)
      puts_if_not_empty($stderr, stderr_output)

      raise "Failed to execute command: #{command}"
    end
  end

  def update_spinner
    return if dry_run?
    spinner.update(time_elapsed: " (#{time_elapsed})")
    spinner.spin
  end

  def stop_spinner(status)
    spinner.update(time_elapsed: '')
    spinner.public_send(status, duration_header)
  end

  def time_elapsed
    case duration.to_i
    when 0..59         then "#{duration.to_i}s ago"
    when 59..119       then '1 minute ago'
    when 120..3599     then "#{duration.to_i / 60} minutes ago"
    when 3600..86_399  then "#{(duration.to_f / (60 * 60)).round(1)} hours ago"
    else "#{(duration.to_f / (60 * 60 * 24)).round(1)} days ago"
    end
  end

  def write_if_ready(from_io, to)
    to << from_io.read_nonblock(1024) while from_io.ready? && !from_io.eof?
  end

  def puts_if_not_empty(io, output)
    io.puts(output) unless output.empty?
  end
end
