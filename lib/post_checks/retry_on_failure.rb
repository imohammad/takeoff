# frozen_string_literal: true

# Simple retry post-check
module PostChecks
  class RetryOnFailure
    def initialize(error_message, attempts: 1, step:)
      @error_message = error_message
      @attempts = attempts
      @step = step
    end

    def run
      return unless @error_message

      1.upto(@attempts) do |attempt|
        return unless @step.error

        puts "Retrying #{@step.class.name} - Attempt #{attempt} - Error: #{@step.error}"

        @step.restart
      end

      abort(@step.error) if @step.error
    end
  end
end
