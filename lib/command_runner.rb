# frozen_string_literal: true

module CommandRunner
  # Runs a command with a fancy title and progress spinner.
  #
  # Upon success no output is displayed, unless silencing of STDOUT has been
  # disabled; in which case the output is written to STDOUT.
  #
  # Upon an error both the output of STDOUT and STDERR of the command is displayed
  # (in their respective output streams).
  #
  # command - The command to execute.
  # title - The title to display, defaults to the command.
  # silence_stdout - When set to `false` the output in STDOUT will be displayed.
  #
  # Returns the output of STDOUT and STDERR of the executed process. Raises an
  # error upon the command not executing properly.
  def run_with_progress(command, title: nil, silence_stdout: true)
    runner = ProgressRunner.new(command, title: title, silence_stdout: silence_stdout)
    runner.execute(verbose: Takeoff.config[:verbose], dry_run: Takeoff.config[:dry_run])
  end

  # Runs a command on all hosts with the given Chef role.
  #
  # roles - A list of roles to run the command on.
  # command - The command to execute.
  def run_command_on_roles(roles, command, *args)
    search_query = Array(roles).reject { |role| role.strip.empty? }.map { |role| "roles:#{role}" }.join(' OR ')
    raise "Command '#{command}' got an empty list of roles, arguments are #{args}. aborting execution" if search_query.empty?
    run_with_progress(
      "bundle exec knife ssh -e -a ipaddress '#{search_query}' '#{command}'",
      *args
    )
  end
end
