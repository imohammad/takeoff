# frozen_string_literal: true

require './lib/steps/base_roles'

class StepRunner
  def initialize(version, environment, repo)
    @version = version
    @environment = environment
    @repo = repo
    @queue = generate_queue
  end

  def generate_queue
    Takeoff.steps.each_with_object([]) do |step, queue|
      klass = Steps[step.keys.first]

      queue << if klass < Steps::BaseRoles
                 klass.new(roles, step_options(step))
               else
                 klass.new
               end
    end
  end

  def run
    @queue.each(&:run!)
  end

  def roles
    @roles ||= Roles.new(@environment)
  end

  def step_options(step)
    value_array = step.values.flatten

    step_config.select { |key, _value| value_array.include?(key.to_s) }.tap do |options_hash|
      options_hash['pre_checks'] = value_array.first['pre_checks']
      options_hash['post_checks'] = value_array.first['post_checks']
    end
  end

  def step_config
    {
      version: @version,
      environment: @environment,
      repo: @repo,
      roles_to_start: roles.regular_and_blessed
    }
  end
end
