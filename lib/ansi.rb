# frozen_string_literal: true

module ANSI
  BOLD = "\e[1m%s\e[0m"
  RED = "\e[1m\e[31m%s\e[0m\e[0m"
end
