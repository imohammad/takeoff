# frozen_string_literal: true

require './lib/steps/base_roles'

module Steps
  class ServicesRestart < BaseRoles
    # Skip unicorn restart on this node
    CUSTOM_RESTART_NODE = 'gitlab-base-fe-web'

    def run
      if restart_custom_services?
        custom_services.each { |service| restart_services(service) }
      else
        restart_services
      end
    end

    private

    def role
      @options[:role]
    end

    def restart_custom_services?
      role == CUSTOM_RESTART_NODE
    end

    def custom_services
      roles.services_for_role(role) - ['unicorn']
    end

    def restart_services(service = '')
      run_command_on_roles role,
                           "sudo gitlab-ctl restart #{service}".strip,
                           title: "Restarting #{service_text(service)} on #{role}"
    end

    def service_text(service)
      service.empty? ? 'services' : service
    end
  end
end
