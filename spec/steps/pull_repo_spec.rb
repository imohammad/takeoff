# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/pull_repo'

describe Steps::PullRepo do
  before { enable_dry_run }

  it 'checkouts the master branch' do
    expect { described_class.new.run }.to output(/git checkout master/).to_stdout_from_any_process
  end

  it 'pulls the latest master' do
    expect { described_class.new.run }.to output(%r{git pull git@gitlab.com:gitlab-org\/takeoff.git master}).to_stdout_from_any_process
  end
end
